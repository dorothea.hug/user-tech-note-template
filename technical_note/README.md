# Instruction and templates for Technical Notes

## Reading time

The suggested read time for the technical note is **5min**. [Here's a convertor from words -> time](https://wordstotime.com/).

## Templates
We have provided you with templates and instructions on how to structure your technical note. <br>
[Rmd with custom CSS template](https://gitlab.com/rconf/user-tech-note-template/-/blob/main/technical_note/technical_note.Rmd) | [Rmd using [`distill`](https://github.com/rstudio/distill)](https://gitlab.com/rconf/user-tech-note-template/-/blob/main/technical_note/technical_note_distill.Rmd)

## Structure
We suggest the following structure to help draft your technical note
- Short abstract
- Introduction
- Findings
- Methods
- Other info/sub-sections
- References

### Contact us
Please let us know if you have any questions.
useR2021 [ at ] r-project.org



